package com.xnsio.cleancode;

import static java.lang.Math.min;
import static java.util.Arrays.asList;
import static java.util.stream.IntStream.range;

public class MeetingAssistent {

    private final int startSlot;
    private final int endSlot;

    public static final int NO_SLOT = -1;

    public MeetingAssistent(int startSlot, int endSlot) {
        this.startSlot = startSlot;
        this.endSlot = endSlot;
    }

    public int getFirstFreeSlot(String date, int beforeSlot, Employee... attendees) {
        return range(startSlot, min(beforeSlot, endSlot))
                .filter(slot -> isSlotFreeForAllAttendies(date, slot, attendees))
                .findFirst()
                .orElse(NO_SLOT);
    }

    private boolean isSlotFreeForAllAttendies(String date, int slotNumber, Employee... attendees) {
        return attendees != null && attendees.length > 0 && asList(attendees).stream()
                .allMatch(employee -> employee.isFree(date, slotNumber));
    }
}
