package com.xnsio.cleancode;

import java.util.HashMap;
import java.util.Map;

public class Employee {

    private Map<String, Map<Integer,Boolean>> calendar = new HashMap<>();

    public boolean isFree(String date, int slotNumber) {
        return getCalendarForDate(date).getOrDefault(slotNumber, true);
    }

    private Map<Integer, Boolean> getCalendarForDate(String date) {
        return calendar.computeIfAbsent(date, dt -> new HashMap<>());
    }

    public void book(String date, int slotNumber) {
        if(!isFree(date, slotNumber)) throw new RuntimeException("Already booked");
        getCalendarForDate(date).put(slotNumber, false);
    }

}
