package com.xnsio.cleancode;

import org.junit.Before;
import org.junit.Test;

import static com.xnsio.cleancode.MeetingAssistent.NO_SLOT;
import static org.junit.Assert.assertEquals;

public class MeetingAssistentTest {

    private String targetDate = "2019-01-01";

    private final Employee MINI = new Employee();
    private final Employee BRAD = new Employee();
    private final Employee JANET = new Employee();
    private final Employee FRANK = new Employee();
    private MeetingAssistent meetingAssistent;

    @Before
    public void setUp() {
        this.meetingAssistent = new MeetingAssistent(8, 17);
    }

    @Test
    public void noSlotForNoAtttendies() {
        assertEquals(NO_SLOT, meetingAssistent.getFirstFreeSlot(targetDate, 17));
    }

    @Test
    public void firstSlotForFreeCalendars() {
        assertEquals(8, meetingAssistent.getFirstFreeSlot(targetDate, 17, MINI, BRAD, JANET, FRANK));
    }

    @Test
    public void firstFreeSlotForMiniAndFrankWhenFrankIsBusyFor8To13() {
        FRANK.book(targetDate, 8);
        FRANK.book(targetDate, 9);
        FRANK.book(targetDate, 10);
        FRANK.book(targetDate, 11);
        FRANK.book(targetDate, 12);
        FRANK.book(targetDate, 13);
        assertEquals(14, meetingAssistent.getFirstFreeSlot(targetDate, 17, MINI, FRANK));
    }

    @Test
    public void noFreeSlotForBradAndJanetBefore10WhenJanetIsBusyUnti11() {
        JANET.book(targetDate, 8);
        JANET.book(targetDate, 9);
        JANET.book(targetDate, 10);
        JANET.book(targetDate, 11);
        assertEquals(NO_SLOT, meetingAssistent.getFirstFreeSlot(targetDate, 10, JANET, BRAD));
    }

    @Test
    public void firstFreeSlotShouldbe12ForMiniAndBradWhenMiniIsFreeAfter11AndBradisBusyAfter13() {
        MINI.book(targetDate, 8);
        MINI.book(targetDate, 9);
        MINI.book(targetDate, 10);
        MINI.book(targetDate, 11);

        BRAD.book(targetDate, 13);
        BRAD.book(targetDate, 14);
        BRAD.book(targetDate, 15);
        BRAD.book(targetDate, 16);
        BRAD.book(targetDate, 17);

        assertEquals(12, meetingAssistent.getFirstFreeSlot(targetDate, 17, MINI, BRAD));
    }

}
